import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class AppiumTestPoc {

    @Test
    public void searchStore() throws MalformedURLException, InterruptedException {
        File folderPath = new File("src/test/resources/");
        File appName = new File(folderPath, "bunnings-staging.apk");

        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "Pix2Ver9");
        capabilities.setCapability(MobileCapabilityType.APP, appName.getAbsolutePath());
        capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME,"uiautomator2");
        capabilities.setCapability(AndroidMobileCapabilityType.APP_WAIT_ACTIVITY, "com.bunnings.retail.*");

        AndroidDriver<AndroidElement> driver = new AndroidDriver<>(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        WebElement skipButton = driver.findElement(By.xpath("//android.widget.TextView[@text='Skip']"));
        skipButton.click();

        WebElement manuallyChooseStoreButton = driver.findElement(By.xpath("//android.widget.TextView[@text='Manually choose a store']"));
        manuallyChooseStoreButton.click();

        WebElement postCodeOrSuburbTextField = driver.findElement(By.xpath("//android.widget.EditText[@text='Postcode or suburb']"));
        postCodeOrSuburbTextField.sendKeys("Port Melbourne");

        WebElement storeSearchResult = driver.findElement(By.xpath("//android.widget.TextView[@text='Port Melbourne']"));
        storeSearchResult.click();

        WebElement selectStoreButton = driver.findElement(By.xpath("//android.widget.TextView[@text='Altona']"));
        selectStoreButton.click();

        WebElement skipToLoginButton = driver.findElement(By.xpath("//android.widget.TextView[@text='Skip for now']"));
        skipToLoginButton.click();
       // Assert.assertTrue("Skip for now to login button is not present", skipToLoginButton.isDisplayed());

        WebElement searchKeywordInput = driver.findElement(By.xpath("//android.widget.TextView[@text='Search for a product']"));
        Assert.assertTrue("Search Keyword field is not displayed", searchKeywordInput.isDisplayed());
        searchKeywordInput.click();

        WebElement searchTextField = driver.findElement(By.xpath("//android.widget.EditText[@text='Search']"));
        searchTextField.sendKeys("hammer");

        WebElement selectSearchKeyword = driver.findElement(By.xpath("//android.widget.TextView[@text='hammer']"));
        selectSearchKeyword.click();

        WebElement suggestedAislesLabel = driver.findElement(By.xpath("//android.widget.TextView[@text='Suggested aisles']"));
        Assert.assertTrue("Suggested aisles are not displayed", suggestedAislesLabel.isDisplayed());
//        WebElement listTab = driver.findElement(By.xpath("//android.widget.TextView[@text='Lists']"));
//        listTab.click();

//        Thread.sleep(10000);


    }

}
